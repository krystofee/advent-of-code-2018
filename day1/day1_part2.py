seen = {0}
result = None
acc = 0

with open('input.txt') as file:
    while result is None:
        print('rewind')
        file.seek(0)
        for line in file.readlines():
            line = int(line)
            acc += line

            if acc in seen:
                result = acc
                break
            seen.add(acc)

print(result)
