declare -i sum=0

while read line; do
    sum=$(($sum + $line))
done < "$1";

echo "$sum"
