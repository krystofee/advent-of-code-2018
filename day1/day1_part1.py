acc = 0
with open('input.txt') as file:
    for line in file.readlines():
        acc += int(line)

print(acc)
