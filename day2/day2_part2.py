lines = []
with open('input.txt') as file:
    lines = file.readlines()


def common(s1, s2):
    s = ''
    for c1, c2 in zip(s1, s2):
        if c1 == c2:
            s += c1
    return s


counter = 0;
for line in lines:
    counter += 1
    for line2 in lines[counter:]:
        common_str = common(line, line2)
        if len(common_str) == len(line) - 1:
            print(common_str)
