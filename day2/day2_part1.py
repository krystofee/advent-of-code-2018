lines = []
with open('input.txt') as file:
    lines = file.readlines()

two_count = 0
three_count = 0

for line in lines:
    has_two = False
    has_three = False
    for char in set(line):
        if line.count(char) == 2:
            has_two = True
        elif line.count(char) == 3:
            has_three = True
    two_count += 1 if has_two else 0
    three_count += 1 if has_three else 0

print(two_count * three_count)
